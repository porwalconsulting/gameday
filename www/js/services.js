angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Andrew Jostlin',
    lastText: 'Did you get the ice cream?',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  }
})

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [{
    id: 0,
    name: 'Ben Sparrow',
    notes: 'Enjoys drawing things',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    notes: 'Odd obsession with everything',
    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
  }, {
    id: 2,
    name: 'Andrew Jostlen',
    notes: 'Wears a sweet leather Jacket. I\'m a bit jealous',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    notes: 'I think he needs to buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    notes: 'Just the nicest guy',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];


  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  }
})
/**
 * Service for app's local database
 */
.factory('LocalDB', function($cordovaSQLite, $rootScope) {
  var db = {};
  return {
    dbh: function() {
      if (window.cordova) {   
        
        db = $cordovaSQLite.openDB({name: "my.db"});
        
      } else {
        
        db = window.openDatabase("my", '1', 'my', 1024 * 1024 * 100); // browser
        
      }
      
      return db;
    },
    
    makechats: function() {
      
      if (window.cordova) {   
        
        $rootScope.db = $cordovaSQLite.openDB("my.db");
        
      } else {
        
        $rootScope.db = window.openDatabase("my.db", '1', 'my', 1024 * 1024 * 100); // browser
      }
      
      $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS   \
                             chats (chat_id unsigned integer,  \
                             conversation_id unsigned integer, \
                             sid unsigned integer, \
                             rid unsigned integer, \
                             sender_fullname varchar(255) DEFAULT NULL, \
                             sender_pic varchar(255) DEFAULT NULL, \
                             recipient_fullname varchar(255) DEFAULT NULL, \
                             recipient_pic varchar(255) DEFAULT NULL, \
                             msg varchar(255) DEFAULT NULL, \
                             read_at DATETIME DEFAULT NULL, \
                             created_at DATETIME)")
        .then(function(res) { 
        
        } , function (err) {
        console.error(err);
      });

    }
  }

})
;
