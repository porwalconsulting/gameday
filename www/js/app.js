function dd(x) { console.log(JSON.stringify(x)); }
// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'ngStorage','ngResource','emguo.poller', 'monospaced.elastic'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('signup', {
    url: "/signup",
    templateUrl: "templates/signup.html"
  })
  .state('test', {
    url: '/test/:accountId',
    templateUrl: 'templates/test.html',
  })
    .state('nogps', {
    url: "/nogps",
    templateUrl: "templates/nogps.html"
  })
    .state('nonet', {
    url: "/nonet",
    templateUrl: "templates/nonet.html"
  })
  .state('setup', {
    url: "/setup",
    templateUrl: "templates/setup.html"
  })
  .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html",
      data: {
        requireLogin: true // this property will apply to all children of 'app'
      }      
  })

  // Each tab has its own nav history stack:

  .state('tab.matches', {
    url: '/matches',
    views: {
      'tab-matches': {
        templateUrl: 'templates/tab-matches.html',
        controller: 'MatchesCtrl'
      }
    }
  })
  .state('tab.match-profile', {
    url: '/matches/:accountId',
    views: {
      'tab-matches': {
        templateUrl: 'templates/match-profile.html',
        controller: 'MatchProfileCtrl'
      }
    }
  })
 .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId/:participents',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })
   .state('tab.friends', {
      url: '/friends',
      views: {
        'tab-friends': {
          templateUrl: 'templates/tab-friends.html',
          controller: 'FriendsCtrl'
        }
      }
    })
  .state('tab.friend-detail', {
      url: '/friend/:friendId',
      views: {
        'tab-friends': {
          templateUrl: 'templates/friend-detail.html',
          controller: 'FriendDetailCtrl'
        }
      }
  })
  
.state('tab.requests', {
  url: '/requests',
    views: {
      'tab-requests': {
        templateUrl: 'templates/tab-requests.html',
        controller: 'RequestsCtrl'
      }
    }
  })
  
.state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
          
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/signup');
  
})
.run(function($ionicPlatform, $rootScope,$state,$cordovaGeolocation, $localStorage,$cordovaSQLite, LocalDB) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    
  
    if(!$rootScope.db) {
        LocalDB.makechats();
    }
    
  });
    
  $rootScope.IntervalId = 0;
  
  $rootScope.dbchats = [];  
  
  if($rootScope.dbchats.length == 0)
  {
      $localStorage.last_chatId = 0;
  }

  $rootScope.loop = function(conversation_id){

      var query = "select * from chats where conversation_id = ?";
  
       if($localStorage.last_chatId > 0 )
       {
            query = "select * from chats where chat_id > ? and conversation_id = ?";
            
            $cordovaSQLite.execute($rootScope.db, query, [$localStorage.last_chatId, conversation_id ]).then(function(res) {
              
                 for(var i = 0; i < res.rows.length; i++)
                 {
                      $rootScope.dbchats.push(res.rows.item(i));

                      $localStorage.last_chatId = res.rows.item(i).chat_id;
                 }  
            }, function (err) {
                console.error(err);
            }); 
       }
       else
       {
          $cordovaSQLite.execute($rootScope.db, query, [conversation_id]).then(function(res) {

              for(var i = 0; i < res.rows.length; i++)
              {
                    $rootScope.dbchats.push(res.rows.item(i));

                    $localStorage.last_chatId = res.rows.item(i).chat_id;
              }

            }, function (err) {
                console.error(err);
            }); 
       }
  }

  $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams){
      var account = window.localStorage.getItem('account');
      
      if(account) {
        if(!$localStorage.account) {
          $localStorage.account = JSON.parse(window.localStorage.getItem('account'));
        }
      }

//    if( (account == null) && toState.name != 'signup') && (toState.name != 'test') ) {
//        e.preventDefault();
//        $state.go('signup');
//    }
      
    if( account && toState.name == 'signup'  ) {
        e.preventDefault();
        $state.go('tab.dash');
    }

    // console.log($localStorage.account);
    if(account && toState.name != 'nogps'){
        if(!$localStorage.account.lat) {
            if(!$localStorage.account.long) {
                //alert('no gps check');
                e.preventDefault();
                $state.go('nogps');
            }
        }
    }
      
      if($localStorage.account && $localStorage.account.lat && toState.name != 'setup'){
//    alert('checking if setup page is needed');
        //console.log($localStorage.account);
        if(!$localStorage.account.sport ||
          !$localStorage.account.proficiency ||
          !$localStorage.account.time_of_play) {
            //alert('loading setup page');
            e.preventDefault();
            $state.go('setup');
        }
    }

  });    
});
