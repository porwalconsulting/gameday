angular.module('starter.controllers', [])

.constant("APP", {
//    "apihost": "http://45.56.104.50"
    "apihost": "http://192.168.1.55:8080"
})

.controller('SignupCtrl', function($scope, $http, $cordovaOauth, $state,$localStorage, APP,poller,$rootScope) {
    poller.stopAll();
    
    window.clearInterval($rootScope.IntervalId);
  
    $scope.$storage = $localStorage;
  
    $scope.facebookLogin = function() {
      
        $cordovaOauth.facebook("903361093018014", ["email","public_profile"]).then(function(result) {

            var postdata = {network: "facebook", access_token: result.access_token, expires_in: result.expires_in ,'t': new Date().getTime() };
            
            $http.post(APP.apihost+'/account', postdata).
            
            success(function(data, status, headers, config) {

                $scope.$storage.account = data;
                window.localStorage.setItem('account', JSON.stringify(data));
                $state.go('tab.dash');
                
            }).
            error(function(data, status, headers, config) {
                  alert(data);
                  alert(status);

            });
            
        }, function(error) {
            alert('fb oauth error');
            alert(JSON.stringify(error));
        });
    }

    $scope.googleLogin = function() {
      
        window.plugins.googleplus.login(
            {},
            function (obj) {
              var postdata = {network: "google", access_token: obj.oauthToken, email:obj.email };
              $http.post(APP.apihost+'/account', postdata).
                success(function(data, status, headers, config) {

                  $scope.$storage.account = data;
                  window.localStorage.setItem('account', JSON.stringify(data));
                  $state.go('tab.dash');
                }).
                error(function(data, status, headers, config) {
                  alert(JSON.stringify(data));               
                });              
//              document.querySelector("#image").src = obj.imageUrl;
//              document.querySelector("#image").style.visibility = 'visible';
//              document.querySelector("#feedback").innerHTML = "Hi, " + obj.displayName + ", " + obj.email;
            },
            function (msg) {
              alert('google oauth error');
              alert(JSON.stringify(msg));
            }
        );        

      
//        $cordovaOauth.google("27276655605-mqspqaemvml69eaepjqe70rkv1bn5lu3.apps.googleusercontent.com", [ "email","https://www.googleapis.com/auth/plus.login"]).then(function(result) {
//            var postdata = {network: "google", access_token: result.access_token };
//          
//            $http.post(APP.apihost+'/account', postdata).
//              success(function(data, status, headers, config) {
//                
//                $scope.$storage.account = data;
//                window.localStorage.setItem('account', JSON.stringify(data));
//                $state.go('tab.dash');
//              }).
//              error(function(data, status, headers, config) {
//                alert(JSON.stringify(data));               
//              });
//            
//        }, function(error) {
//            alert('google oauth error');
//            alert(JSON.stringify(error));
//        });        
    }

})

.controller('NogpsCtrl', function($scope, $state, $cordovaGeolocation, $localStorage,poller,$rootScope) {
  
    poller.stopAll();
  
    window.clearInterval($rootScope.IntervalId);
    $scope.$storage = $localStorage;
    
    $scope.refresh = function(){
        $cordovaGeolocation
        .getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
        .then(function (position) {
          $scope.$storage.account.lat = position.coords.latitude;
          $scope.$storage.account.long = position.coords.longitude;
            $state.go('tab.dash');
        }, function(err) {

        });
    }
})

.controller('NonetCtrl', function($scope, $http, $state, $cordovaGeolocation, $localStorage,poller,$rootScope) {
  
  
    poller.stopAll();
  
    window.clearInterval($rootScope.IntervalId);
    $scope.$storage = $localStorage;

    $scope.refresh = function(){
      $http.get(APP.apihost)
        .success(function(data){
            $state.go('tab.dash');
        });
    }
})
  
.controller('RequestsCtrl', function($scope, $http, $state,$stateParams, $localStorage,APP,poller,$cordovaSQLite,$rootScope) {

  
    poller.stopAll();
    window.clearInterval($rootScope.IntervalId);
    $scope.$storage = $localStorage;

    
    $http.get(APP.apihost+"/request/received/"+$scope.$storage.account.id)
        .success(function(data, status, headers, config){
            $scope.data = data;
    });
  
    
    $scope.confirm_request = function(id)
    {
       var postdata = {id : id };
      
       $http.post(APP.apihost+"/request/confirm",postdata)
              .success(function(data, status, headers, config){
         
       var insert = "INSERT INTO chats (chat_id,sid,rid,conversation_id,sender_fullname,sender_pic,recipient_fullname,recipient_pic,msg,read_at,created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
         
              $cordovaSQLite.execute($rootScope.db, insert, [data.id,data.sender,data.recipient,data.conversation_id,data.fullname,
                                                   data.pic,$scope.$storage.account.fullname,$scope.$storage.account.pic,
                                                   data.msg,data.read_at,data.created_at]).then(function(res) {
              }, function (err) {
                console.error(err);
              }); 
         
            var result = { chatId:data.conversation_id, participents:data.sender+'-'+data.recipient };
            $state.go('tab.chat-detail', result);
       }); 
    }
    
    $scope.reject_request = function(id)
    {
       var postdata = {id : id };
      
       $http.post(APP.apihost+"/request/reject",postdata)
              .success(function(data, status, headers, config){
      });
    }
})

.controller('TestCtrl', function($scope, $http, $state,$stateParams, $localStorage,APP,poller, $rootScope) {
  
    poller.stopAll();
  
    window.clearInterval($rootScope.IntervalId);
  
    $scope.$storage = $localStorage;

    $http.get(APP.apihost+"/account/"+$stateParams.accountId)
        .success(function(data, status, headers, config){
            $scope.$storage.account = data;
            $state.go('tab.dash');
    });
})

.controller('SetupCntrl', function($scope, $http, $state, $cordovaGeolocation, $localStorage,APP,poller,$rootScope) {
  
    poller.stopAll();
  
    window.clearInterval($rootScope.IntervalId);
  
    $scope.$storage = $localStorage;

    $scope.settings = {
        sports: ['Cricket', 'Tennis', 'Football'] ,
        proficiencies: ['Beginner', 'Intermediate', 'Professional'] ,
        timeblocks: ['Morning', 'Evening', 'Anytime'] ,
    };

    $scope.updateSettings = function(){
        $http.put(APP.apihost+'/account/'+$scope.$storage.account.id, $scope.$storage.account).
          success(function(data, status, headers, config) {
            //console.log(data);
          }).
          error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });
    }

    $scope.next = function() {

        if($localStorage.account.sport &&
          $localStorage.account.proficiency &&
          $localStorage.account.time_of_play) {
            $state.go('tab.dash');
        } else {
            alert("Please fill all the fields");
        }

    }
})

.controller('DashCtrl', function($scope, $state, $http, $cordovaGeolocation, $localStorage,poller,$rootScope, APP) {
  
  
    poller.stopAll();
  
    window.clearInterval($rootScope.IntervalId);
    
    $scope.$storage = $localStorage;
    
    $scope.account = JSON.parse(window.localStorage.getItem('account'));

    $scope.logout = function(){
        window.localStorage.removeItem('account');
        $scope.$storage.account = null;

        $state.go('signup');
    }
    
    $scope.localert = function(){

        $cordovaGeolocation
        .getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
        .then(function (position) {
          $scope.$storage.account.lat = position.coords.latitude;
          $scope.$storage.account.long = position.coords.longitude;
          
          $http.put(APP.apihost+'/account/'+$scope.$storage.account.id, $scope.$storage.account)
          
        }, function(err) {
          alert(err);
        });    
    }
})

.controller('MatchesCtrl', function($scope, $http,$state,$localStorage,APP,poller,$rootScope) {
  
  poller.stopAll();
  
  window.clearInterval($rootScope.IntervalId);
  
  $scope.$storage = $localStorage;
  
  var url = APP.apihost+'/matches?uid='+$scope.$storage.account.id;

  if ($scope.$storage.account) {
  $scope.Math = window.Math;
  $scope.parseInt = parseInt;

  $http.get(url)
    .success(function(data){
        $scope.data = data;
    });
  }

  $scope.refresh = function(){
     $http.get(url)
        .success(function(data){
            $scope.data = data;
        });
    }

    $scope.view = function(match) {
        $state.go('tab.match-profile',{accountId:match.id});
    }
})

.controller('MatchProfileCtrl', function($scope, $http, $state,$stateParams, $localStorage,APP,poller,$rootScope) {
  
  poller.stopAll();
  
  window.clearInterval($rootScope.IntervalId);
  
  $scope.$storage = $localStorage;
  $scope.account = [];
  
  $http.get(APP.apihost+"/account/"+$stateParams.accountId)
        .success(function(data, status, headers, config){
        $scope.account = data;
        $state.go('tab.match-profile');
  });
  
  $scope.sent_request = function(to){

    var postdata = {from : $scope.$storage.account.id, to : to };
       
    $http.post(APP.apihost+"/request",postdata)
         .success(function (data,success,headers,config){
        alert("A request was sent!");
    });  
  }
  
  $scope.add_myplayers = function(to){
    
    var postdata = { from : $scope.$storage.account.id, to : to };
    
    $http.post(APP.apihost+"/myplayers",postdata)
         .success(function (data,success,headers,config){
      
        alert('Add Player');
    });
  }
  
  
  $scope.report = function(reported_user_id){
    
    var postdata = { reporting_user_id : $scope.$storage.account.id, reported_user_id : reported_user_id };
    
    $http.post(APP.apihost+"/report",postdata)
         .success(function (data,success,headers,config){
      
        alert('Your report has been submitted succesfully !');
    });
    
  }
  
  $scope.block = function(blocked_user_id){
    
    var postdata = { blocking_user_id : $scope.$storage.account.id, blocked_user_id : blocked_user_id };
    
    $http.post(APP.apihost+"/block",postdata)
         .success(function (data,success,headers,config){
      
        alert('You have blocked succesfully !');
    });
    
  }
  
})
  
.controller('ChatsCtrl', function($scope, Chats,$localStorage,$http,APP,poller,$rootScope) {
  
  poller.stopAll();
  
  window.clearInterval($rootScope.IntervalId);
  
  $scope.$storage = $localStorage;
 
  $http.get(APP.apihost+"/conversation?p="+$scope.$storage.account.id)
       .success(function(data,status,headers,config){
         $scope.chats = data;
  });
  
  $scope.remove = function(chat) {
    Chats.remove(chat);
  } 
  
})

.controller('ChatDetailCtrl', function($scope, $state, $stateParams,Chats,$http,APP,$localStorage,
                                       $cordovaSQLite,$resource,$ionicScrollDelegate,poller,$rootScope) {
  
  if (!$localStorage.hasOwnProperty('last_chatId')) {
    $localStorage.last_chatId = 0;    
  }
  
//     poller.stopAll();
  
  $scope.chats = $rootScope.dbchats;
  
  $rootScope.IntervalId = window.setInterval($rootScope.loop,3000,[$stateParams.chatId]);
  
  $scope.$storage = $localStorage;

  $scope.conversation_id = $stateParams.chatId;

  $scope.msg = '';
  
  $scope.participents = $stateParams.participents.split('-');
  
  var url = APP.apihost+"/chats"; 
  
  // Define your resource object.
  var myResource = $resource(url,{cid:$stateParams.chatId,recipient_id:$scope.$storage.account.id,type:'unread'});
  
  // Create and start poller.
  var myPoller = poller.get(myResource,{action:'query',smart:true,delay:3000});
  
//    Update view. Most likely you only need to define notifyCallback.
   myPoller.promise.then(function(data){
    alert(data);
   }, 
    null,
    function(data){
        
      var read_ids = [];

      $cordovaSQLite.execute($rootScope.db, "select chat_id from chats where read_at is null", []).then(function(res) {

        if(res.rows.length) {
          
          for(var i = 0; i < res.rows.length; i++)
          {
               read_ids.push(res.rows.item(i).chat_id);
          }
          
          $http.get(APP.apihost+"/chats?chats_ids="+read_ids)
           .success(function (data,success,headers,config){

              angular.forEach(data,function(v){
                $cordovaSQLite.execute($rootScope.db,'update chats  set read_at = ? where chat_id = ?', [v.read_at,v.id]).then(function(res) {

                }, function (err) {
                  console.error(err);
                });
              });
          });             
        }

      },function (err) {
        console.error(err);
      });
     
     
        if(data.length > 0)
        {
            var insert = "INSERT INTO chats (chat_id,sid,rid,conversation_id,sender_fullname,sender_pic,recipient_fullname,recipient_pic,msg,read_at,created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
          
          angular.forEach(data,function(v){
          
            $cordovaSQLite.execute($rootScope.db,'select chat_id from chats where chat_id = ?', [v.id]).then(function(res) {
                
              if(res.rows.length == 0)
              {
                   $cordovaSQLite.execute($rootScope.db, insert, [v.id,v.sender,v.recipient,v.conversation_id,v.fullname,
                                                      v.pic,$scope.$storage.account.fullname,$scope.$storage.account.pic,
                                                      v.msg,v.read_at,v.created_at]).then(function(res) {
                    }, function (err) {
                      console.error(err);
                    });    
              }
            }); 
            
        });
          
        var ids = [];
        
        angular.forEach(data,function(v){
            ids.push(v.id);
        });
        
        $http.get(APP.apihost+"/chats/"+ids+"/markread")
             .success(function (data,success,headers,config){
            
            angular.forEach(data,function(v){
              $cordovaSQLite.execute($rootScope.db,'update chats  set read_at = ? where chat_id = ?', [v.read_at,v.id]).then(function(res) {
            
              }, function (err) {
                console.error(err);
              });
            });
        });        
      }
        $ionicScrollDelegate.scrollBottom(true);
   });

   
   $scope.goChats = function(){$state.go('tab.chats');}

   $scope.send = function(msg)
   {
     if(msg.length > 0)
     {
        var explode = $stateParams.participents.split('-');
        
        if($scope.$storage.account.id == explode[0])
        {
            var postdata = {sender_id : $scope.$storage.account.id,
                            recipient_id : explode[1],
                            message:msg,
                            conversation_id:$scope.conversation_id,
                            timeout: 1000};
        }
        else
        { 
            var postdata = {sender_id : $scope.$storage.account.id,
                            recipient_id : explode[0],
                            message:msg,
                            conversation_id:$scope.conversation_id,
                            timeout: 1000};
        }
       
        $http.post(APP.apihost+"/chats",postdata)
             .success(function (data,success,headers,config){
          
          
               var insert = "INSERT INTO chats (chat_id,sid,rid,conversation_id,sender_fullname,sender_pic,recipient_fullname,recipient_pic,msg,read_at,created_at) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
          
             $cordovaSQLite.execute($rootScope.db,'select chat_id from chats where chat_id = ?', [data.id]).then(function(res) {
                
              if(res.rows.length == 0)
              {
                   $cordovaSQLite.execute($rootScope.db, insert, [data.id,data.sender,data.recipient,data.conversation_id,$scope.$storage.account.fullname,
                                                  $scope.$storage.account.pic,data.fullname,data.pic,
                                                  data.msg,data.read_at,data.created_at]).then(function(res) {
            
                    }, function (err) {
                      console.error(err);
                    });    
              }
             });
          
              $ionicScrollDelegate.scrollBottom(true);
        });    
     }
   }
   
})

.controller('FriendsCtrl', function($scope, Friends,poller,$rootScope,$http,APP,$localStorage) {
  
  poller.stopAll();
  
  $scope.$storage = $localStorage;
  
  window.clearInterval($rootScope.IntervalId);

//  $scope.friends = Friends.all();
  
  $http.get(APP.apihost+"/myplayers?sender_id="+$scope.$storage.account.id)
       .success(function (data,status,headers,config){
    
        $scope.friends = data;     
    
  });
})

.controller('FriendDetailCtrl', function($scope, $stateParams, $localStorage, Friends,poller,$rootScope,$http,APP) {
  poller.stopAll();
  
  window.clearInterval($rootScope.IntervalId);
  
  $scope.$storage = $localStorage;
  
  $http.get(APP.apihost+"/myplayers/"+$stateParams.friendId)
       .success(function (data,status,headers,config){
    
       $scope.friend = data;    
    
  });
})

.controller('AccountCtrl', function($scope, $state, $localStorage, $http,APP,poller,$rootScope,$cordovaSocialSharing,$cordovaDialogs) {
  poller.stopAll();
  
  window.clearInterval($rootScope.IntervalId);
  
  $scope.$storage = $localStorage;
  $scope.discoverable = 0;
  $scope.enquiry = '';
  
  $scope.settings = {
    enableFriends: true,
    sports: ['Cricket', 'Tennis', 'Football'] ,
    proficiencies: ['Beginner', 'Intermediate', 'Professional'] ,
    timeblocks: ['Morning', 'Evening', 'Anytime'] ,
    distance_unit: ['Km', 'Miles'] ,
  };
    
  $scope.updateSettings = function(){
    
    $http.put(APP.apihost+'/account/'+$scope.$storage.account.id, $scope.$storage.account).
      success(function(data, status, headers, config) {
        //console.log(data);
      }).
      error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }
  
  $scope.update_enquiry = function(e){
    
    var postdata = {account_id:$scope.$storage.account.id,msg:e};
    
    if(e != '')
    {
        $http.post(APP.apihost+'/enquiry',postdata).
            success(function(data, status, headers, config) {
              
            }).
            error(function(data, status, headers, config) {
              // called asynchronously if an error occurs
              // or server returns response with an error status.
        });
    }
    
  }
  
  $scope.share = function(){
    $cordovaSocialSharing
                    .share('Share Sportizzy with your friends and get free matches ;)', 'Sportizzy Vikas Yojana', null,'http://j.mp/sportz');
    
  }
  
  $scope.logout = function(){
      $cordovaDialogs.confirm('Press Ok to logout', 'Are you sure ?', ['Ok','Cancel'])
    .then(function(buttonIndex) {
      
        if(buttonIndex == 1)  
        {
            window.localStorage.removeItem('account');
            $scope.$storage.account = null;

            $state.go('signup');
        }
    });
  }
  
  $scope.delete = function(){
      $cordovaDialogs.confirm('Press Ok to delete your account', 'Are you sure ?', ['Ok','Cancel'])
    .then(function(buttonIndex) {

        if(buttonIndex == 1)  
        {
            alert('We won\'t let you go so easily. Nop Nop Nop nop Nop.');
        }
    });
    
    
  };    
});
